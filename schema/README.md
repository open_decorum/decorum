# Decorum Schema

Schemas define how clients and servers should communicate through the API.

The schemas here are written in [YAML][yaml] to the [OpenAPI 3.0][openapi]
specification, which means that the OpenAPI tools will work with them.

[yaml]: https://yaml.org/
[openapi]: https://github.com/OAI/OpenAPI-Specification

## Editing and Viewing

Copy and paste the contents of the schema file into the [Swagger
Editor][editor].

[editor]: https://editor.swagger.io/

If you have Docker installed on your computer, you can use the editor locally:

    docker pull swaggerapi/swagger-editor
    docker run -d -p 8080:8080 swaggerapi/swagger-editor

Then visit <http://localhost:8080/> in your browser.

## License

The Decorum schemas are licensed under the [Apache 2.0][apache-2.0] license.
See the [LICENSE](LICENSE) file for full details.

[apache-2.0]: https://www.apache.org/licenses/LICENSE-2.0.html
