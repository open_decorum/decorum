# Open Decorum

_Conversations on your schedule._

Your time and attention are precious.  Modern communication has made it
incredibly convenient to send a message to someone, but this takes a toll on our
most precious resource.  Anyone with access can now send you a text, chat
message, or email that triggers a near instantaneous notification that demands
your attention.

Open Decorum aims to compliment your current methods of communication by giving
you a channel of communication that happens at your pace so you can engage when
you are at your best.

## Vision

**Conversations start as threads.**  Chats can be hard to follow when multiple
topics are brought up before you can respond; this will even happen between two
people. Some chats support turning a message into a threaded conversation, but
the default is to still have a linear flow of conversation.  By making threads
the base unit of conversation, you can simply create a new thread for each new
topic that gets brought up.  This ensures that the subjects you bring up can be
responded to without getting lost in a flood of new topics.

**Notifications on your schedule.**  When someone replies to a thread you are in
or you are invited to join a new thread, you aren't immediately notified.
Instead, new events are checked on a schedule you setup for yourself and
a notification is only created when you have new activity.  For example, you may
set your client to check every three hours between when you finish your morning
routine and before bed.  You will be able to consciously check for updates
yourself, but not with a quick frequency that would allow you to immediately
respond to every message.

Everyone communicating with you in this manner will not expect an immediate
response.  If someone _needs_ an immediate response, they can reach you in the
plethora of tools they already have, such as text or chat.  This allows
you to have meaningful conversations with people who have different schedules
than yourself; even different time zones.  Most communication does not need your
immediate attention, but we have the instinct to immediately check
notifications.  By having most conversations happen at your pace, you free more
of your attention and your ability to focus.

**Threads expire.** If there is no activity on a thread it will eventually
expire.  This provides an easy mechanism for your old threads to be
automatically cleaned.  Many conversations don't need to be kept and the act of
"forgetting" has the benefit of keeping what's relevant easy to find.

**Threads can be exported.**  For the conversations that you want to keep, you
can export them.  Whether you want to document the full context of how you've
arrive at an important decision, or working on a narrative with a friend, you
should be able to pull a thread out and keep it.  You'll be warned when a thread
is about to expire with plenty of time to decide which threads you want to keep.

## FAQ

**Q:** How can I start using Open Decorum?

**A:** Open Decorum is under heavy development.  We have not made a release yet.

**Q:** Who hosts Open Decorum?

**A:** You or a friend will be able to host Open Decorum on your own server.

**Q:** Will Open Decorum support multiple accounts/identities?

**A:** We plan to support this by allowing you to login to multiple accounts
with your Open Decorum client.  This way you can have an account that's for
work, a personal account, and possibly others if you want one for each circle of
friends.

**Q:** How is Open Decorum different from email?

**A:** Email used to be the way to communicate on your own schedule when people
used dial-up to check their email.  You could send an email and the person
receiving it would get it when they had time to consciously sit at their computer
and dialed a phone number to connect to the internet.  Compare that to now when
we typically have phone constantly connected to the internet and default to
giving you a notification as soon as you receive an email.  Open Decorum brings
the conversation back to your pace.

Since email has become instantaneous, people tend to have expectations that you
will read their email shortly after they send it.  Open Decorum prevents this by
making it impossible to check for new messages frequently.

Email is also often rife with automated messages.  Many conversations have moved
away from email.  Open Decorum is intended for human-to-human conversations.

## Community / Contributing

If you are excited about Open Decorum and would like to contribute, we have
multiple ways in which you can participate.  You do not need to code to
contribute!

Here are a few ways you can help us:

- Feedback
- Documentation
- Testing
- Bug reports
- Code

_Note:_ We are still working on how we wish to collaborate, so these tools and
how we use these tools are subject to change.

## License

Licensed under the [EUPL-1.2](https://choosealicense.com/licenses/eupl-1.2).
See the [LICENSE](LICENSE) file for full details.
